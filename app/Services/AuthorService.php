<?php

namespace App\Services;
use App\Traits\ConsumesExternalService;

class AuthorService{
    use ConsumesExternalService;

    public function __construct(){
        $this->baseUri = config('services.authors.base_uri');
    }

    public function obtainAuthors(){
        return $this->performRequest('GET','/authors');
    }
}