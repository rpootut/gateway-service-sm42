<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Services\AuthorService;

class AuthorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

     public $authorService;

    public function __construct(AuthorService $authorService)
    {
        $this->authorService = $authorService;
    }

    public function index(){
        $authors = $this->authorService->obtainAuthors();
        return $authors;
    }

    public function show($author){
        
    }

    public function store(Request $request){
        
    }

    public function update(Request $request, $author){
        
    }

    public function destroy($author){
        
    }
}
